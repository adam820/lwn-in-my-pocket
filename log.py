import logging
import sys
from os import environ

import daiquiri
import daiquiri.formatter

# Set up logging
daiquiri.setup(
    level=environ.get("LOGLEVEL", logging.INFO),
    outputs=(
        daiquiri.output.Stream(
            sys.stderr,
            formatter=daiquiri.formatter.ColorExtrasFormatter(
                fmt=daiquiri.formatter.DEFAULT_FORMAT
            ),
        ),
        daiquiri.output.File(
            directory=".",
            formatter=daiquiri.formatter.ColorExtrasFormatter(
                fmt=daiquiri.formatter.DEFAULT_FORMAT
            ),
        ),
    ),
)
logger = daiquiri.getLogger(__name__)
