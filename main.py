import re
import sys
from os import environ

import requests
from bs4 import BeautifulSoup

from log import logger
from sack import Sack


# https://lwn.net/SubscriberLink/MakeLink
# POST with param 'articleid'


def preflight_checks() -> None:
    """
    Check for environment variables and other items before starting
    """
    # TODO: Move these from being ENV vars to CLI args
    if not environ.get("POCKET_KEY"):
        logger.critical("Please set your Pocket Consumer Key via environment variable: POCKET_KEY")
        sys.exit(1)


class LWN:
    """
    Class representing the site contents of LWN
    """
    def __init__(self):
        # Get the content from the LWN page
        self.url = "https://www.lwn.net"
        self.LOGGED_IN = False
        self.headline_links = []
        self.session = requests.Session()
        self.lwn_user = environ.get("LWN_USER", None)
        self.lwn_pass = environ.get("LWN_PASS", None)

        # Handle Log-In
        if self.lwn_user is None and self.lwn_pass is None:
            logger.warning("No LWN username/password provided, not logging in")
            logger.warning("To log in, set environment variables LWN_USER and LWN_PASS")
        elif self.lwn_user is not None and self.lwn_pass is not None:
            # Attempt to log in
            logger.info("Attempting to log in...")
            response = self.session.post(
                url=f"https://lwn.net/Login/",
                data={"Username": self.lwn_user, "Password": self.lwn_pass},
            )
            if "Log out" in str(response.content):
                logger.info("...logged in successfully")
                self.LOGGED_IN = True
            else:
                logger.error("Unable to log in, invalid username or password")

        self.page_content = self.get_page_content()

    def get_page_content(self):
        logger.info("Getting the LWN page content")
        lwn_html = self.session.get(self.url).content
        page_content = BeautifulSoup(lwn_html, features="html.parser")
        return page_content

    def parse_articles(self):
        # Clear the headline links list
        self.headline_links = []

        # Get all headlines from front page
        all_hlines = self.page_content.find_all("h2", class_="Headline")
        if len(all_hlines) <= 1 or all_hlines is None:
            logger.error("Unable to locate headlines. Please check your internet connection.")
            sys.exit(1)

        # Loop headlines and grab link to the full article story
        for hline in all_hlines:
            all_links = hline.find_next_sibling().find_all("a")
            if len(all_links) >= 1:
                # Grab the last link, whether full story or comments, and strip
                # comments from the link
                link = all_links[-1]['href']
                link = re.sub(r"#Comments", "", link)
                article_id = link.split("/")[2]

                # If the link is for a subscriber-only article, we'll need to generate
                # a free subscriber link (for personal use only, naturally), but to allow
                # Pocket to synchronzize it's contents on our behalf... but only if we're logged in.
                if re.match(r"^\[\$].*", hline.text) and self.LOGGED_IN:
                    sublink_page = self.session.post(
                        url="https://lwn.net/SubscriberLink/MakeLink",
                        data={"articleid": article_id}
                    )

                    SUBMATCH = r".*(https://lwn.net/SubscriberLink/\d+/[a-z0-9]+/).*"

                    for line in sublink_page.text.splitlines():
                        sublink_search = re.search(SUBMATCH, line)
                        if sublink_search is not None:
                            # TODO: It would be great to split on the variable for the URL here
                            # but (is this a bug?) it doesn't work. Splitting on the variable
                            # self.url just returns the whole URL again...?
                            link = sublink_search.group(1).split("https://lwn.net")[-1]

                            # Since we're going to store the resolved title (if possible) in Pocket,
                            # strip the [$] from the headline to avoid repeat synchs
                            hline_text = re.sub(r"^\[\$]\s", "", hline.text)
                            self.headline_links.append((hline_text, f"{self.url}{link}", article_id))
                            break
                    continue

                # Article wasn't a subscriber-only link
                hline_text = re.sub(r"\s+\[LWN.net]", "", hline.text)
                self.headline_links.append((hline_text, f"{self.url}{link}", article_id))
            else:
                self.headline_links.append((hline.text, None, None))

        logger.info(f"Got {len(self.headline_links)} total articles")

        # If we're not logged in, remove articles that are subscriber-only
        for article in self.headline_links.copy():
            if not self.LOGGED_IN:
                if re.match(r"^\[\$].*", article[0]):
                    if article in self.headline_links:
                        logger.warning(f"Not logged in, removing: {article[0]}")
                        self.headline_links.remove(article)
                        continue
            # Not handling Weekly Edition posts
            if re.match(r".*Weekly\sEdition.*", article[0]):
                if article in self.headline_links:
                    logger.debug("Removing Weekly Edition")
                    self.headline_links.remove(article)
                    continue
            # Not handling Security Update posts
            if re.match(r"Security\s+[U|u]pdates", article[0]):
                if article in self.headline_links:
                    logger.debug("Removing Security Updates")
                    self.headline_links.remove(article)
                    continue


if __name__ == "__main__":
    # Run preflight checks
    preflight_checks()
    lwn = LWN()
    lwn.parse_articles()

    sack = Sack(consumer_key=environ.get("POCKET_KEY"))
    sack.cache.clean("lwn.net")
    sack.add_articles(lwn.headline_links)
