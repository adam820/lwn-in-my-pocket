import json
from json import JSONEncoder, JSONDecoder
import sys
from datetime import datetime
from datetime import timedelta
from os import environ
from pathlib import Path
from typing import Any, Optional, Union

import pocket as p

from log import logger


class EncoderWithDateTime(JSONEncoder):
    """
    Subclasses the default JSONEncoder to allow ISO export
    of datetime objects automatically.
    """

    def default(self, o: Any) -> Any:
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return JSONEncoder.default(self, o)


class DrawString:
    """
    File for keeping track of what was already synchronized

    site: {
        article_id: {
            title,
            date_fetch,
            date_sync
        }
    }
    """

    filepath = Path(environ.get("SYNC_FILE", "./sack.json"))
    data = None

    def __init__(self):
        self.load()

    def is_cached(self, article_id, site=None) -> bool:
        """
        Checks whether the article is cached and returns boolean
        :param article_id: The article ID
        :param site: (optional) The site; if no site provided, all sites will be checked
        :return: boolean
        """
        a_id = str(article_id)
        if site:
            if site in self.data.keys():
                for k, v in self.data[site].items():
                    if str(k) == a_id:
                        return True
                else:
                    return False
            else:
                logger.error(f"No site '{site}' found in the cache")
                return False
        else:
            # Check all sites
            for site in self.data.keys():
                for k, v in self.data[site].items():
                    if str(k) == a_id:
                        return True
            return False

    def save(self):
        try:
            with open(self.filepath, "w") as filedata:
                logger.info(f"Saving sync cache data to {str(self.filepath)}")
                filedata.write(json.dumps(self.data, cls=EncoderWithDateTime, indent=2, sort_keys=True))
        except PermissionError as e:
            logger.error(f"Couldn't open {str(self.filepath): permission denied}")
        except (OSError, SystemError) as err:
            logger.error(f"Couldn't open {str(self.filepath)}: {err}")

    def load(self):
        if self.filepath.exists():
            try:
                with open(self.filepath, "r") as filedata:
                    logger.info("Loading sync cache data...")
                    self.data = json.loads(filedata.read())
            except PermissionError as e:
                logger.error(f"Couldn't open {str(self.filepath): permission denied}")
            except (OSError, SystemError) as err:
                logger.error(f"Couldn't open {str(self.filepath)}: {err}")
        else:
            logger.info("Existing cache file does not exist; starting with empty data")
            self.data = {}

    def add(
        self,
        site: str,
        article_id: [str, int],
        title: str,
        date_fetch: [datetime, str],
        date_sync: [datetime, str],
    ):
        if site not in self.data.keys():
            self.data[site] = {}
        if article_id not in self.data[site].keys():
            logger.debug(f'Adding article ID "{article_id}": {title} to sync file')
            self.data[site][str(article_id)] = {
                "title": title,
            }

            # Make sure the date is in isoformat string
            if isinstance(date_fetch, datetime):
                date_fetch = date_fetch.isoformat()
            if isinstance(date_sync, datetime):
                date_sync = date_sync.isoformat()

            self.data[site][str(article_id)]["date_fetch"] = date_fetch
            self.data[site][str(article_id)]["date_sync"] = date_sync
        else:
            logger.debug(f'Article ID "{article_id}" already exists in sync file.')

    def delete(self, site: str, article_id: [str, int]):
        if article_id in self.data[site].keys():
            del self.data[site][article_id]

    def clean(self, site: str, days: int = 5):
        try:
            if site in self.data.keys():
                if len(self.data[site]) > 0:
                    # Check how fresh the article is; if older than the time delta,
                    # scrub it from the dict
                    threshold = datetime.utcnow() - timedelta(days)
                    logger.info(f"Cleaning cached article listings in {site} older than {str(threshold)}")
                    new_dict = {
                        article: details for article, details in self.data[site].items()
                        if threshold < datetime.fromisoformat(details['date_sync'])
                    }

                    # Compare the results and save the cache file
                    if new_dict != self.data[site]:
                        self.data[site] = new_dict
                        self.save()
                else:
                    logger.warning(f"No articles found for {site}")
        except (KeyError, TypeError) as err:
            logger.error(f"Issue cleaning article: {err}")


class Sack:
    articles = None
    api_remaining = None
    _titles = []
    cache = DrawString()

    def __init__(self, consumer_key: str) -> None:
        """
        The Pocket connector for adding LWN articles to Pocket

        :param consumer_key: The Pocket API Consumer Key
        """
        self.consumer_key = consumer_key
        self.auth_token = self.get_pocket_authtoken()
        self.instance = p.Pocket(
            consumer_key=self.consumer_key, access_token=self.auth_token
        )
        self.fetch_articles()

    def get_pocket_authtoken(self) -> str:
        """
        Using your consumer key, get an authentication token after authorizing the application via browser.
        If already authorized and cached, use that instead

        :return: Authentication token
        """
        logger.debug("Checking for Pocket API token cache file")

        auth_token_file = Path().home() / ".limp_token_cache.txt"
        if auth_token_file.exists():
            logger.info("Pocket API token cache file exists, loading from file")
            with open(auth_token_file, "r") as token_file:
                self.auth_token = token_file.read()
        else:
            logger.info(
                "Pocket API token cache file does not exist, will generate a new one"
            )
            request_token = p.Pocket.get_request_token(consumer_key=self.consumer_key)
            auth_url = p.Pocket.get_auth_url(
                code=request_token, redirect_uri="https://getpocket.com"
            )

            # TODO: Based on the platform, spawn the browser automatically; total hack here
            input(
                f"Navigate to the following link to authorize this application to access your account, then "
                f"press a button to continue: {auth_url}"
            )

            # Get the access token via API
            try:
                user_creds = p.Pocket.get_credentials(
                    consumer_key=self.consumer_key, code=request_token
                )
            except (p.AuthException, p.PocketException):
                logger.critical(
                    "Unable to authenticate to Pocket; please make sure Consumer Key is authorized"
                )
                sys.exit(1)

            self.auth_token = user_creds["access_token"]

            try:
                with open(auth_token_file, "w") as token_file:
                    logger.info(
                        f"Saving access token to cache file: {str(auth_token_file)}"
                    )
                    token_file.write(self.auth_token)
            except PermissionError:
                logger.critical("Permission denied while saving token cache file")
                sys.exit(126)
            except (OSError, SystemError):
                logger.critical("Unable to save token cache file")
                sys.exit(1)

        return self.auth_token

    def fetch_articles(self, tag: str = "lwn.net") -> None:
        try:
            logger.debug("Fetching existing Pocket articles...")
            pocket_articles = self.instance.get(detailType="complete", tag=tag)
        except (p.RateLimitException, p.PocketException) as e:
            logger.error(e)
            return None

        # Update the articles list
        self.articles = pocket_articles[0]["list"]

        # Update the reference titles list
        for k, v in self.articles.items():
            try:
                if v["resolved_title"] is None or v["resolved_title"] == "":
                    if v["given_title"] not in self._titles:
                        self._titles.append(v["given_title"])
                else:
                    if v["resolved_title"] not in self._titles:
                        self._titles.append(v["resolved_title"])
            except KeyError:
                logger.warning(f"No resolved title for article {k}")

    def _add_article(
            self,
            title: str,
            article_id: Union[str, int],
            url: str,
            site: str = "lwn.net",
            tags: list = "lwn.net"
    ) -> None:
        """
        Add an article to Pocket

        :param site: The site origin site base URL, e.g. 'lwn.net'
        :param title: Article title
        :param article_id: The article ID
        :param url: Article URL
        :param tags: A list of tags to add to the article
        :return: None
        """
        # Set the default tags
        if tags is None:
            tags = [site]

        if site not in tags:
            tags.append(site)

        try:
            self.instance.add(title=title, url=url, tags=tags)
            self.cache.add(site, article_id, title, datetime.utcnow().isoformat(), datetime.utcnow().isoformat())
        except (p.RateLimitException, p.PocketException, p.AuthException) as e:
            logger.error(e)
            return None

        self.fetch_articles()

    def add_articles(self, articles: list) -> None:
        """
        Adds a list of articles to Pocket. Articles should be a tuple of (title, url, article_id).

        :param articles: List of articles
        :return: None
        """
        for article in articles:
            if not isinstance(article, tuple):
                logger.warning(f"Article is not a tuple: {str(article)}")
                continue
            else:
                if article[0] not in self._titles:
                    #TODO: This is a hack and a half; support the site
                    if "lwn.net" not in self.cache.data.keys():
                        self.cache.data["lwn.net"] = {}

                    # Check the cache before trying to add; if not
                    # add the article and cache it
                    if article[2] not in self.cache.data["lwn.net"].keys():
                        logger.info(f"Synchronizing article: {article[0]}")
                        self._add_article(title=article[0], url=article[1], article_id=article[2])
                    else:
                        # Inform the user that the site isn't in Pocket, but since it's
                        # in the cache, it has been synchronized previously
                        date_sync = self.cache.data['lwn.net'][article[2]]['date_sync']
                        logger.warn(
                            f"Article '{article[2]}' is not in Pocket, but was previously synced at"
                            f" {date_sync} -- '{article[0]}'")
                else:
                    logger.info(f"Article already exists in Pocket: {article[0]}")
                    continue

        # Save the cache file
        self.cache.save()
